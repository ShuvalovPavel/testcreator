package my.numberaddition;

import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class passingTestPage extends javax.swing.JFrame {

    private Integer userScore;
    private Integer numberCurrentPage;
    private String fName;
    private Integer maxScore;
    private ArrayList<page> test = new ArrayList();
    private ArrayList<result> results = new ArrayList();

    public Integer chose;
    private JScrollPane scrollPane;
    private Integer currentVal = 0;
    private page currentPage = new page();
    private Integer check = 0;//проверка, что вариант выбран
    private Point location;

    private class RadioItemListener implements ItemListener {

        public void itemStateChanged(ItemEvent e) {
            boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
            AbstractButton button = (AbstractButton) e.getItemSelectable();
            if (selected) {
                String str = button.getActionCommand();
                for (int i = 0; i < currentPage.variants.size(); i++) {
                    if (currentPage.variants.get(i) == str) {
                        currentVal = currentPage.variantsWeight.get(i);
                        check = 1;
                    }
                }
            }
        }
    }

    public passingTestPage() {
    }

    public passingTestPage(ArrayList<page> te, ArrayList<result> re, Integer uS, Integer nCP, String fN, Integer mS) {
        initComponents();
        test = te;
        results = re;
        System.out.println("te=" + te + "re=" + re);
        userScore = uS;
        numberCurrentPage = nCP;
        fName = fN;
        maxScore = mS;
        currentPage = test.get(numberCurrentPage);
        ButtonGroup bG = new ButtonGroup();
        JPanel newPanel = new JPanel();
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        final Font font = new Font("Verdana", Font.PLAIN, 12);
        JTextArea addTextArea;
        addTextArea = new JTextArea(2, 40);
        addTextArea.setLineWrap(true);
        addTextArea.setWrapStyleWord(true);
        addTextArea.setText(currentPage.question);
        addTextArea.setEditable(false);
        addTextArea.setFont(font);
        JLabel addImg = new JLabel();
        ImageIcon ii = new ImageIcon(currentPage.img);
        addImg.setIcon(new ImageIcon(ii.getImage().getScaledInstance(360, 240, ii.getImage().SCALE_DEFAULT)));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(addTextArea))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(addImg))
        );
        scrollPane = new JScrollPane(newPanel);
        ArrayList<String> vars = currentPage.variants;
        JButton addButton1 = new JButton("далее");
        JButton addButton2 = new JButton("выйти");
        for (int i = 0; i < vars.size(); i++) {
            JRadioButton buttonVariant = new JRadioButton(vars.get(i));
            bG.add(buttonVariant);
            ((JPanel) scrollPane.getViewport().getView()).add(buttonVariant);
            newPanel.setLayout(new BoxLayout(newPanel, BoxLayout.Y_AXIS));

            ItemListener itemListener = new RadioItemListener();
            buttonVariant.addItemListener(itemListener);
        }
        addButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (check == 0) {
                    showMessageDialog(null, "выберете вариант");
                } else {
                    userScore = userScore + currentVal;
                    if (numberCurrentPage < test.size()) {

                        location = passingTestPage.this.getLocation();
                        passingTestPage formPassingTestPage = new passingTestPage(test, results, userScore, numberCurrentPage, fName, maxScore);
                        formPassingTestPage.setLocation(location);
                        formPassingTestPage.setVisible(true);
                    } else {
                        Point location;
                        location = passingTestPage.this.getLocation();
                        summation formSummation = new summation(results, userScore);
                        formSummation.setLocation(location);
                        formSummation.setVisible(true);
                    }
                    setVisible(false);
                }
            }
        });
        addButton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(addTextArea)
                        .addComponent(addImg)
                        .addComponent(scrollPane)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(addButton1)
                                .addComponent(addButton2))
                )
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(addTextArea)
                .addComponent(addImg)
                .addComponent(scrollPane)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(addButton1)
                        .addComponent(addButton2))
        );

        numberCurrentPage++;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 386, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 444, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(passingTestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(passingTestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(passingTestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(passingTestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new passingTestPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
